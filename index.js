//load our libraries (always load library this way)
var express = require("express");
var path = require("path");

//Be careful when defining variable as other libraries might be using the same name
//Use as little variables as possible to prevent collison

//console.log(">>> express: " +express);
//console.log(">>>path: "+path);

//create an instance of express application (always different depending on the library)
var app = express();

//Define routes
//asking the library express to serve the static content found in the public directory
app.use(express.static(__dirname+"/public")); //use defines a route
//console.log(">> dirname: "+__dirname);

app.use(function(req,resp){
    resp.status(404);
    resp.type("text/html"); //representation
    resp.send("<h1>File not file</h1><p>The current time is " + new Date()+"</p>");
});


//setting the port as a property of the app
//set defines the attribute e.g. 3000 for port
app.set("port",3000);
console.log("Port:"+app.get("port"));

//start the server on the specified port
//server is going to listen to the port
app.listen(app.get("port"),function(){
    console.log("Application is listening on port" + app.get("port"))
});







